<?php
/**
 * Implements hook_form_system_theme_settings_alter() function.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function thor_form_system_theme_settings_alter(&$form, $form_state) {
  /*
   * Create the form using Forms API
   */
  $form['colours'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Colours setting'),
  );
  $form['colours']['thor_demo_colours'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use colours to demo column layout'),
    '#default_value' => theme_get_setting('thor_demo_colours' ),
  );
}
