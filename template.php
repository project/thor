<?php
function thor_preprocess_html(&$variables, $hook) {
  if(theme_get_setting('thor_demo_colours')){
      drupal_add_css(
        $variables['directory'] . '/stylesheets/colours.css',
        array(
          'group' => CSS_THEME,
          'every_page' => TRUE,
        )
      );
  }
}
